
import 'package:redux_conceptions/base_network_service.dart';
import 'package:redux_conceptions/services/network_service.dart';

class UnsplashImageService {
  UnsplashImageService._privateConstructor();

  static final UnsplashImageService instance =
      UnsplashImageService._privateConstructor();
  static String _url = 'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

  Future<BaseNetworkService> getImages() async {
    var responseData =
        await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}
