import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:redux_conceptions/base_network_service.dart';

enum HttpType {
  post,
  get,
  put,
  delete,
  patch,
}

class NetworkService {
  NetworkService._privateConstructor();

  static final NetworkService instance = NetworkService._privateConstructor();

  Future<BaseNetworkService> request(
    HttpType type,
    String url,
    Map<String, dynamic> body,
    Map<String, dynamic> headers,
  ) async {

    http.Response response;

    if (type == HttpType.get) {
      response = await http.get(url, headers: headers);
    }
    return BaseNetworkService(error: '', response: json.decode(response.body));
  }



}
