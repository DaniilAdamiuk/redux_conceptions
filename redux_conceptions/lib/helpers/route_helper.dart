

import 'package:flutter/material.dart';
import 'package:redux_conceptions/res/consts.dart';
import 'package:redux_conceptions/ui/first_page/first_page.dart';
import 'package:redux_conceptions/ui/second_page/second_page.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';
  RouteHelper._privateConstructor();
  static final RouteHelper _instance = RouteHelper._privateConstructor();
  static RouteHelper get instance => _instance;
  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_FIRST:
        return _defaultRoute(
          settings: settings,
          page: FirstPage(),
        );
      case ROUTE_SECOND:
        return _defaultRoute(
          settings: settings,
          page: SecondPage(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: FirstPage(),
        );
    }
  }
  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}