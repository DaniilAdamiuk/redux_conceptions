import 'package:flutter/material.dart';

class UnsplashProfileImageDto {
  final String small;
  final String medium;
  final String large;

  UnsplashProfileImageDto({
   @required this.small,
   @required this.medium,
   @required this.large,
  });

  factory UnsplashProfileImageDto.fromJson(Map<String, dynamic> json) {
    return UnsplashProfileImageDto(
      small: json["small"],
      medium: json["medium"],
      large: json["large"],
    );
  }
//

}
