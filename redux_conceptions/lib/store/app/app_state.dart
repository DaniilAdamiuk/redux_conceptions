import 'package:flutter/foundation.dart';
import 'package:redux_conceptions/store/pages/counter_page_state/counter_page_state.dart';
import 'package:redux_conceptions/store/pages/photos_state/photos_epics.dart';
import 'package:redux_conceptions/store/pages/photos_state/photos_state.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final CounterPageState counterPageState;
  final PhotosState photosState;


  AppState({
    @required this.counterPageState,
    @required this.photosState,
  });

  factory AppState.initial() {
    return AppState(
      counterPageState: CounterPageState.initial(),
      photosState: PhotosState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterPageState: state.counterPageState.reducer(action),
      photosState: state.photosState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    PhotosEpics.indexEpic,
  ]);
}