
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/store/app/app_state.dart';

class NavigationSelector {
  static void Function(String route) navigateToNextPage(Store<AppState> store) {
    return (String route) => store.dispatch(NavigateToAction.replace(route));
  }
}
