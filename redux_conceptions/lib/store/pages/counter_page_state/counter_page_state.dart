import 'dart:collection';

import 'package:redux_conceptions/store/app/reducer.dart';
import 'package:redux_conceptions/store/pages/counter_page_state/counter_page_actions.dart';

class CounterPageState {
  final int counter;


  CounterPageState({
    this.counter,
  });

  factory CounterPageState.initial() {
    return CounterPageState(
      counter: 0,
    );
  }

  CounterPageState copyWith({
   int counter,
  }) {
    return CounterPageState(
     counter: counter ?? this.counter,
    );
  }

  CounterPageState reducer(dynamic action) {
    return Reducer<CounterPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
      }),
    ).updateState(action, this);
  }

  CounterPageState _incrementCounter() {
    return CounterPageState(counter: counter+1);
  }

  CounterPageState _decrementCounter() {
    return copyWith(counter: counter-1);
  }

}