
import 'package:redux/redux.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/pages/counter_page_state/counter_page_actions.dart';

class CounterPageSelectors {

  static int getCounterValue (Store<AppState> store) {
    return store.state.counterPageState.counter;
  }

  static void Function() getIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() getDecrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }
}