import 'dart:collection';

import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/store/app/reducer.dart';
import 'package:redux_conceptions/store/pages/photos_state/photos_action.dart';


class PhotosState {
  final List<ImageDto> images;
  final ImageDto selectedImage;
  final String selectedId;

  PhotosState(
    this.images,
    this.selectedId,
      this.selectedImage,
  );

  factory PhotosState.initial() => PhotosState([], '', ImageDto());

  PhotosState copyWith({List<ImageDto> images, String id, ImageDto image,}) {
    return PhotosState(
      images ?? this.images,
      id ?? this.selectedId,
      image ?? this.selectedImage,
    );
  }

  PhotosState reducer(dynamic action) {
    return Reducer<PhotosState>(
      actions: HashMap.from({
        SaveImagesAction: (dynamic action) =>
            saveImages((action as SaveImagesAction).images),
        SelectImageAction: (dynamic action) =>
            selectImage((action as SelectImageAction).id),
      }),
    ).updateState(action, this);
  }

  PhotosState saveImages(List<ImageDto> images) {
    return copyWith(images: images);
  }

  PhotosState selectImage(String id) {
    ImageDto image = images.firstWhere((image) => image.id == id);
    return copyWith(id: id, image: image);
  }
}
