import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/repsitories/unsplash_image_repository.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'file:///C:/Projects/redux_conceptions/redux_conceptions/redux_conceptions/lib/store/shared/empty_action.dart';
import 'package:redux_conceptions/store/pages/photos_state/photos_action.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class PhotosEpics {
  static final indexEpic = combineEpics<AppState>([
    getImagesEpic,
    setImagesEpic,
  ]);

  static Stream<dynamic> getImagesEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetImagesAction>().switchMap(
      (action) {
        return Stream.fromFuture(
          UnsplashImageRepository.instance.getImages().then(
            (List<ImageDto> images) {
              if (images == null) {
                return EmptyAction();
              }
              return SaveImagesAction(
                images: images,
              );
            },
          ),
        );
      },
    );
  }

  static Stream<dynamic> setImagesEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<SaveImagesAction>().switchMap(
      (action) {
        return Stream.fromIterable(
          [
            EmptyAction(),
          ],
        );
      },
    );
  }
}
