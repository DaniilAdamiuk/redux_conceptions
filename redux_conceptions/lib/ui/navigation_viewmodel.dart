import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/shared/navigation_selector.dart';

class NavigationViewModel {
  final void Function(String route) changePage;

  NavigationViewModel({@required this.changePage});

  static NavigationViewModel fromStore(Store<AppState> store) {
    return NavigationViewModel(
        changePage: NavigationSelector.navigateToNextPage(store));
  }

}