import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_conceptions/res/consts.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/ui/navigation_viewmodel.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationViewModel>(
      converter: NavigationViewModel.fromStore,
      builder: (BuildContext context, NavigationViewModel viewModel) {
        return Scaffold(
          body: Center(
            child: RaisedButton(
              color: Colors.red,
              child: Text('To Second Page'),
              onPressed: () => viewModel.changePage(ROUTE_SECOND),
            ),
          ),
        );
      },
    );
  }
}
