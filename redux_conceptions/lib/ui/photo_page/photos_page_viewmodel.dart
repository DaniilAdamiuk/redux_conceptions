import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/pages/photos_state/photos_selector.dart';

class PhotosPageViewModel {
  final List<ImageDto> images;
  final void Function() getImages;

  PhotosPageViewModel({
    @required this.images,
    @required this.getImages,
  });

  static PhotosPageViewModel fromStore(Store<AppState> store) {
    return PhotosPageViewModel(
      images: PhotosSelector.getCurrentImages(store),
      getImages: PhotosSelector.getImagesFunction(store),
    );
  }
}
