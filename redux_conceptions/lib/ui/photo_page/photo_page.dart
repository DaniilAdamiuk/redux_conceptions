import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/ui/photo_page/photos_page_viewmodel.dart';

class PhotoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PhotosPageViewModel>(
      converter: PhotosPageViewModel.fromStore,
      onInitialBuild: (PhotosPageViewModel viewModel) => viewModel.getImages(),
      builder: (BuildContext context, PhotosPageViewModel viewModel) {
        return Container(
          child: ListView.builder(
            itemCount: viewModel.images.length,
            itemBuilder: (BuildContext ctx, int index) => Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(50.0),
                child: Container(
                  height: 250.0,
                  margin: const EdgeInsets.all(10.0),
                  child: GridTile(
                    header: GridTileBar(
                      backgroundColor: Colors.black87,
                      title: Text(
                        viewModel.images[index].altDescription ?? '',
                        textAlign: TextAlign.center,
                        maxLines: 2,
                      ),
                    ),
                    child: Image.network(
                      viewModel.images[index].imageUrls.regular,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
