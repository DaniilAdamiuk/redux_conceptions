import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/pages/counter_page_state/counter_page_selectors.dart';

class CounterPageViewModel {
  final int counter;
  final void Function() incrementCounter;
  final void Function() decrementCounter;

  CounterPageViewModel({
    @required this.counter,
    @required this.incrementCounter,
    @required this.decrementCounter,
  });

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
      counter: CounterPageSelectors.getCounterValue(store),
      incrementCounter: CounterPageSelectors.getIncrementCounterFunction(store),
      decrementCounter: CounterPageSelectors.getDecrementCounterFunction(store),
    );
  }
}
